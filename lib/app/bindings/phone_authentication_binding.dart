import 'package:get/get.dart';
import 'package:tracking_ui_flutter/app/controller/phone_authentication/phone_authentication_controller.dart';

class PhoneAuthenticationBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PhoneAuthenticationController>(
        () => PhoneAuthenticationController());
  }
}
