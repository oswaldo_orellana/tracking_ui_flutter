import 'package:get/get.dart';
import 'package:tracking_ui_flutter/app/controller/configuration_profile/configuration_profile_controller.dart';

class ConfigurationProfileBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ConfigurationProfileController>(
        () => ConfigurationProfileController());
  }
}
