import 'package:get/get.dart';
import 'package:tracking_ui_flutter/app/controller/home/home_controller.dart';
import 'package:tracking_ui_flutter/app/controller/mapas/ctrl_mapas_controller.dart';

class HomeBinding implements Bindings {
  @override
  void dependencies() {
    Get.put<CtrlMapasController>(
      CtrlMapasController(),
      permanent: true,
    );

    Get.put<HomeController>(
      HomeController(),
      permanent: true,
    );
  }
}
