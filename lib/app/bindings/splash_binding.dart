import 'package:get/get.dart';
import 'package:tracking_ui_flutter/app/controller/splash/splash_controller.dart';

class SplashBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SplashController>(() => SplashController());
  }
}
