import 'package:get/get.dart';
import 'package:tracking_ui_flutter/app/controller/pedidos/pedidos_controller.dart';
import 'package:tracking_ui_flutter/app/data/provider/remote/pedido_api_client.dart';
import 'package:tracking_ui_flutter/app/data/repository/pedido_client.dart';

class PedidosBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PedidoClient>(
      () => PedidoClient(),
    );
    Get.lazyPut<PedidoRepository>(
      () => PedidoRepository(),
    );
    Get.lazyPut<PedidosController>(() => PedidosController());
  }
}
