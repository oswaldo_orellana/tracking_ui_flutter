abstract class AppTranslation {
  static Map<String, Map<String, String>> translationKeys = {
    'es_BO': esBo,
    'en_US': enUS,
  };
}

final Map<String, String> enUS = {
  // login page begin
  'titlelogin': 'Enter your phone number to log in',
  'textinputnumber': 'Enter the phone number',
  'textinputcode': 'Enter the confirmation code',
  'texttermcond':
      'By clicking <<Next>>, you accept the Terms and Conditions and the privacy policy',
  'next': 'next',
  'verify': 'verify',
  'ok': 'accept',
  'done': 'Done',
  'save': 'Save',
  'titleerror': 'We had a problem',
  'messageerrornumber': 'Please enter a valid number',
  'messageerrorcode': 'wrong verification code',
  // login page end
  // home page begin
  // state page begin
  'statecharging': 'loading ......',
  'statewaiting': 'waiting .....',
  'staterequest': 'requesting .....',
  'stateprogres': 'in process .....',
  'statecanceling': 'canceling request ......',
  'statesuccesfull': 'successful',
  'stateerror': "We had a problem try again",
  // sate page end
  // button home page begin
  'requestmotobutton': 'request motorcycle',
  'cancelrequestbutton': 'cancel request',
  // button home page end
  // message modal the cancell order begin
  'titlemodalcancelorder': 'Cancel Request?',
  'messagemodalcancelorder':
      'If you cancel the application, you will lose trust points. ',
  'cancel': 'cancel',
  // message modal the cancell order end
  // title button sheet begin
  'normaltitle': 'Become a messenger!',
  'requesttitle': 'Waiting for Moto',
  'progresstitle': '!!!! Request in Process !!!!!',
  // title button sheet end
  // text edit of home begin
  'noaddress': 'Address not found',
  'origin': 'Origin',
  'destination': 'Destination',
  'rate': 'Offer your rate',
  'comment': 'Comments and description',
  // text edit of home end
  // show modal origin and destination begin
  'textplaceorigin': 'Pickup location',
  'textplacedestination': 'Destination place',
  'showmap': 'Show on map',
  // show modal origin and destination end
  // home page end
  //
  // select Map page begin
  'titleoriginselect': 'Please indicate the collection point',
  'titledestselect': 'Enter the destination',
  'noaddressselect': 'Address not found \n Click <<next>>',
  'add address': 'Enter an address',
  'titleaddrespagenot':
      'Address not found. Enter the address of the pick-up point. ',
  // select map page end
  // conf profile begin
  'titleprofile': 'My Profile',
  'name': 'Name',
  'lastname': 'Surname',
  'datebirth': 'Date of Birth',
  'email': 'Email',
  'nameempty': 'New User',
  'emailempty': 'Email not found',
  // conf profile end
};

final Map<String, String> esBo = {
  //login page begin
  'titlelogin': 'Ingrese su número de teléfono para iniciar sesión',
  'textinputnumber': 'Ingrese el número de teléfono',
  'textinputcode': 'Introduzca el código de confirmación',
  'texttermcond':
      'Al pulsar <<Siguiente>>, acepta los Terminos y condiciones y la politica de privacidad',
  'next': 'siguiente',
  'verify': 'verificar',
  'ok': 'aceptar',
  'done': 'Hecho',
  'save': 'Guardar',
  'titleerror': 'Tuvimos un problema',
  'messageerrornumber': 'Ingrese un número válido',
  'messageerrorcode': 'código de verificación incorrecto',
  //login page end
  //home page begin
  //state page begin
  'statecharging': 'cargando......',
  'statewaiting': 'esperando.....',
  'staterequest': 'solicitando.....',
  'stateprogres': 'en proceso.....',
  'statecanceling': 'cancelando solicitud......',
  'statesuccesfull': 'exitoso',
  'stateerror': "Tuvimos un problema intente nuevamente",
  //sate page end
  //button home page begin
  'requestmotobutton': 'solicitar moto',
  'cancelrequestbutton': 'cancelar solicitud',
  //button home page end
  //message modal the cancell order begin
  'titlemodalcancelorder': '¿Cancelar Solicitud?',
  'messagemodalcancelorder':
      'Si cancela la solicitud perdera puntos de confianza. ',
  'cancel': 'cancelar',
  //message modal the cancell order end
  //title button sheet begin
  'normaltitle': '¡Conviertete en un mensajero!',
  'requesttitle': 'Esperando Moto',
  'progresstitle': '!!!!Solicitud en Proceso!!!!!',
  //title button sheet end
  //text edit of home begin
  'noaddress': 'Dirección no encontrada',
  'origin': 'Origen',
  'destination': 'Destino',
  'rate': 'Ofrece tu tarifa',
  'comment': 'Comentarios y descripción',
  //text edit of home end
  //show modal origin and destination begin
  'textplaceorigin': 'Lugar de recogida',
  'textplacedestination': 'Lugar de destino',
  'showmap': 'Mostrar en el mapa',
  //show modal origin and destination end
  //home page end
  //
  //select Map page begin
  'titleoriginselect': 'Indique el punto de recogida',
  'titledestselect': 'Indique el destino',
  'noaddressselect': 'Direccion no encontrada \n Haga clic en <<siguiente>>',
  'adddireccion': 'Ingrese una direción',
  'titleaddrespagenot':
      'Dirección no encontrada. Escriba la dirección del punto de recogida.',
  //select map page end
  //conf profile begin
  'titleprofile': 'Mi Perfil',
  'name': 'Nombre',
  'lastname': 'Apellido',
  'datebirth': 'Fecha de Nacimiento',
  'email': 'Correo Electrónico',
  'nameempty': 'Nuevo Usuario',
  'emailempty': 'Email no encontrado',
  //conf profile end
};
