import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:tracking_ui_flutter/app/data/model/user_model.dart';
import 'package:tracking_ui_flutter/app/data/provider/local/local_number.dart';
import 'package:tracking_ui_flutter/app/data/provider/remote/authentication_client.dart';
import 'package:tracking_ui_flutter/app/data/provider/remote/phone_authentication.dart';
import 'package:tracking_ui_flutter/app/data/provider/remote/usuarios_provider.dart';
import 'package:tracking_ui_flutter/app/data/repository/authentication_client.dart';
import 'package:tracking_ui_flutter/app/data/repository/local/local_number_repository.dart';
import 'package:tracking_ui_flutter/app/data/repository/phone_authentication.dart';
import 'package:tracking_ui_flutter/app/data/repository/user_repository.dart';

class DependencyInjection {
  static void init() {
    Get.lazyPut<FlutterSecureStorage>(() => FlutterSecureStorage());
    Get.lazyPut<LocalNumber>(() => LocalNumber());
    Get.put(LocalNumberRepository());

    //authentication phone
    Get.lazyPut<FirebaseAuth>(() => FirebaseAuth.instance);

    Get.lazyPut<AuthenticationClient>(
      () => AuthenticationClient(),
      fenix: true,
    );
    Get.lazyPut<PhoneAuthentication>(
      () => PhoneAuthentication(),
      fenix: true,
    );

    Get.lazyPut<AuthenticationRepository>(
      () => AuthenticationRepository(),
      fenix: true,
    );
    Get.lazyPut<PhoneAuthenticationRepository>(
      () => PhoneAuthenticationRepository(),
      fenix: true,
    );

    //Instance of Usuario
    Get.lazyPut<Usuario>(() => Usuario(), fenix: true);
    //registro user provider and userRepository

    Get.lazyPut<UserProvider>(() => UserProvider(), fenix: true);
    Get.lazyPut<UserRepository>(
      () => UserRepository(),
      fenix: true,
    );
  }
}
