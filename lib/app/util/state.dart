enum Status {
  wait, //loading -> wait the progress
  error, //error -> write error of the connection
  ok, //ok -> connection remote successfull
  none, //isempty
  remove,
}

enum OrderStatus {
  none,
  requesting,
  inProgress,
  finished,
  cancelled,
}
