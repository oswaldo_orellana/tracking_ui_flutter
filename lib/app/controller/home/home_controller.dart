import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tracking_ui_flutter/app/controller/mapas/ctrl_mapas_controller.dart';
import 'package:tracking_ui_flutter/app/data/provider/remote/authentication_client.dart';
import 'package:tracking_ui_flutter/app/data/repository/authentication_client.dart';
import 'package:tracking_ui_flutter/app/data/repository/local/local_number_repository.dart';
import 'package:tracking_ui_flutter/app/data/repository/pedido_client.dart';
import 'package:tracking_ui_flutter/app/routes/app_routes.dart';
import 'package:tracking_ui_flutter/app/util/state.dart';

class HomeController extends GetxController {
  Rx<Status> _status = Rx(Status.none);
  Rx<OrderStatus> statusPedido = Rx(OrderStatus.none);

  final pedidosRepository = Get.find<PedidoRepository>();

  RxBool tipo = true.obs; //true = origen, false = destino
  RxString direccion = "".obs;
  RxString _direccionOrigen = "".obs;
  RxString _direccionDestino = "".obs;
  LatLng origen;
  LatLng destino;
  String idPedido;
  String number;

//SEt y getter de los atributos
  LatLng get getOrigen => this.origen;
  set setOrigen(LatLng origen) => this.origen = origen;
  get getDestino => this.destino;
  set setDestino(LatLng destino) => this.destino = destino;
  String get direccionOrigen => _direccionOrigen.value;
  String get direccionDestino => _direccionDestino.value;
  set direccionOrigen(String origen) => _direccionOrigen.value = origen;
  set direccionDestino(String destino) => _direccionDestino.value = destino;
  get status => this._status.value;
  //SET Y GETTER de los Atributos End

  @override
  void onInit() async {
    _status.value = Status.none;
    await cargarDatos();
    pedidosRepository.orderStatus(number).listen((event) {
      String stado = event;
      changeStatus(stado);
      print("aaaa {$statusPedido } {$stado}");
    });
    super.onInit();
  }

  // PARA LOS PEDIDOS DE LA PAGINA HOME BEGIN
  add() {
    _status.value = Status.wait;
    final titleOrigen = direccionOrigen;
    final titleDestino = direccionDestino;
    final origen = getOrigen;
    final destino = getDestino;
    if (origen == null ||
        destino == null ||
        titleOrigen.isEmpty ||
        titleDestino.isEmpty) {
      _status.value = Status.none;
      failed();
    } else {
      pedidosRepository
          .add(
              titleOrigen: titleOrigen,
              origen: origen,
              destino: destino,
              number: number,
              titleDestino: titleDestino)
          .then((_) {
        idPedido = _;
        statusPedido.value = OrderStatus.requesting;
        _status.value = Status.ok;
        Future.delayed(Duration(seconds: 1))
            .then((value) => _status.value = Status.none);
        print("registro correctamente");
      }).catchError((_) {
        _status.value = Status.error;
        print("ocurrio un error");
      });
    }
  }

  cargarDatos() async {
    number = Get.find<AuthenticationClient>().user.phoneNumber;
    await pedidosRepository.getPedidoUser(number).then((_) {
      print(_.toJson());
      if (_ == null) return;
      idPedido = _.id;
      guardarDireccion(_.origen, _.titleOrigen);
      tipo.value = false;
      guardarDireccion(_.destino, _.titleDestino);
      changeStatus(_.estado);
      print("solo entra una vez aqui");
    }).catchError((_) {
      print("tuvimos un problema al recuperar datos");
    });
  }

  failed() async {
    Get.bottomSheet(
      Container(
        child: Column(
          children: <Widget>[
            Text(
              "Ingrese datos correctamente",
              style: TextStyle(
                fontSize: 25.0,
                color: Colors.red,
              ),
            ),
            // ignore: deprecated_member_use
            RaisedButton(
              onPressed: () {
                Get.back();
              },
              child: Icon(
                Icons.close,
                color: Colors.red,
                size: 30.0,
              ),
            ),
          ],
        ),
      ),
      backgroundColor: Colors.white,
    );
  }
  //PARA LOS PEDIDOS DE LA PAGINA HOME END

  //PARA LOS MAPAS Y RECARGO DE LOS MARKER BEGIN
  guardarDireccion(LatLng position, String title) {
    if (this.tipo.value) {
      direccionOrigen = title;
      setOrigen = position;
    } else {
      direccionDestino = title;
      setDestino = position;
    }
    cargarMarker();
  }

  cargarMarker() {
    if (getOrigen != null && getDestino != null) {
      final map = Get.find<CtrlMapasController>();
      map.cleanMarkerAndPolyline();
      map.addMarker(getOrigen, "ORIGEN", direccionOrigen);
      map.addMarker(getDestino, "DESTINO", direccionDestino);
      map.agregarPolyline(getOrigen, getDestino);
      update(["mapas"]);
    }
  }

  //PARA LOS MAPAS Y EL RECARGO DE LOS MARKER END

  onChangeDireccion(String di) {
    this.direccion.value = di;
    print("este es la direccion : ${this.direccion.value}");
  }

  void signOut() async {
    Get.find<AuthenticationRepository>().signOut();
    Get.find<LocalNumberRepository>().clearNumber();
    Get.offNamed(Routes.LOGINFORM);
  }

  void cancelar() {
    pedidosRepository
        .cancelar(idPedido)
        .then((value) => limpiarDatos())
        .whenComplete(() => _status.value = Status.none);
    _status.value = Status.remove;
  }

  limpiarDatos() {
    _direccionOrigen.value = "";
    _direccionDestino.value = "";
    origen = null;
    destino = null;
    idPedido = "";
    statusPedido.value = OrderStatus.none;
    Get.find<CtrlMapasController>().cleanMarkerAndPolyline();
    update(["mapas"]);
  }

  onchangestado() {
    print(Status.ok);
  }

  // PARA LOS PEDIDOS CHANGE PEDIDO AND GET PEDIDO
  changeStatus(String status) {
    switch (status) {
      case "requesting":
        statusPedido.value = OrderStatus.requesting;
        break;
      case "inProgress":
        statusPedido.value = OrderStatus.inProgress;
        break;
      case ("finished"):
        statusPedido.value = OrderStatus.none;
        break;

      case "cancelled":
        statusPedido.value = OrderStatus.none;
        break;
      default:
    }
  }

  String statePedido() {
    String stat;
    switch (statusPedido.value.index) {
      case 1: //requesting
        stat = "requesting";
        break;
      case 2: // inprogress
        stat = "inProgress";
        break;
      case 3: // finalizado
        stat = "finished";
        break;
      case 4: // cancelado
        stat = "cancelled";
        break;
      default:
    }
    return stat;
  }

  // PARA LOS PEDIDOS CHANGE PEDIDO AND GET PEDIDO

}
