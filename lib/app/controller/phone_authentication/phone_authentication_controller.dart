import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tracking_ui_flutter/app/data/provider/remote/phone_authentication.dart';
import 'package:tracking_ui_flutter/app/data/repository/local/local_number_repository.dart';
import 'package:tracking_ui_flutter/app/data/repository/phone_authentication.dart';
import 'package:tracking_ui_flutter/app/data/repository/user_repository.dart';
import 'package:tracking_ui_flutter/app/routes/app_routes.dart';
import 'package:tracking_ui_flutter/app/util/state.dart';

class PhoneAuthenticationController extends GetxController
    with PhoneAuthenticationMixin {
  final _phoneAuthenticationRepository =
      Get.find<PhoneAuthenticationRepository>();

  final _userRepository = Get.find<UserRepository>();

  RxBool mostrar = false.obs;
  String _number = "+591";
  String _verificationId = "";
  int _resendToken = 0;
  String _smsCode = "";

  final Rx<Status> _status = Rx(Status.none);
  Rx<Status> get status => _status;

  String _error = "";

  void showDialogError() {
    _status.value = Status.error;
    Get.dialog(
      Container(
        child: AlertDialog(
          title: Text('titleerror'.tr),
          content: Text(_error),
          actions: [
            TextButton(
              onPressed: () {
                _status.value = Status.none;
                Get.back();
              },
              child: Text('ok'.tr),
            ),
          ],
        ),
      ),
    );
  }

  void onNumberChanged(String text) {
    _number = "+591" + text;
    print(_number);
  }

  void onSmsCodeChanged(String text) {
    _smsCode = text;
    print(_smsCode);
  }

  Future<void> sendMeACode() async {
    _status.value = Status.wait;
    if (_number.length == 12) {
      await _phoneAuthenticationRepository.verifyPhoneNumber(
        _number,
        resendToken: _resendToken,
      );
    } else {
      _error = 'messageerrornumber'.tr;
      showDialogError();
    }
  }

  void verifyCode() {
    _status.value = Status.wait;
    _phoneAuthenticationRepository.verifySmsCode(
      verificacionId: _verificationId,
      smsCode: _smsCode,
    );
  }

  @override
  void onInit() {
    super.onInit();
    _phoneAuthenticationRepository.addPhoneListener(this);
  }

  @override
  void onCodeAutoRetrievalTimeout(String verificationId) {
    _verificationId = verificationId;
  }

  @override
  void onCodeSent(String verificationId, int resendToken) {
    status.value = Status.none;
    _verificationId = verificationId;
    _resendToken = resendToken;
    mostrar.value = true;
  }

  @override
  void onVerificationCompleted(String idToken) {
    try {
      status.value = Status.ok;
      agregarNumero();
      Get.offNamed(Routes.HOME);
    } catch (e) {
      _error = 'messageerrorcode'.tr;
      showDialogError();
    }
  }

  void agregarNumero() {
    Get.find<LocalNumberRepository>().setNumber(_number);
    _userRepository.addNumber(_number);
  }

  @override
  void onVerificationFailed(String error) {
    _error = 'messageerrorcode'.tr;
    showDialogError();
  }
}
