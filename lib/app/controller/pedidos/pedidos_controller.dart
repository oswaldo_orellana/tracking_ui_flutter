import 'package:get/get.dart';
import 'package:tracking_ui_flutter/app/data/model/pedido.dart';
import 'package:tracking_ui_flutter/app/data/repository/pedido_client.dart';

class PedidosController extends GetxController {
  final pedidosRepository = Get.find<PedidoRepository>();

  Pedido pedido;
  String number;
}
