import 'package:get/get.dart';
import 'package:tracking_ui_flutter/app/data/model/user_model.dart';
import 'package:tracking_ui_flutter/app/data/repository/user_repository.dart';

enum StatusPerfil {
  none,
  wait,
  ok,
  error,
}

class ConfigurationProfileController extends GetxController {
  Usuario _user = Get.find<UserRepository>().user;
  final userRepository = Get.find<UserRepository>();
  Rx<StatusPerfil> status;

  get user => _user;

  @override
  void onInit() {
    status = Rx(StatusPerfil.none);
    super.onInit();
  }

  onChangeName(String name) {
    _user.nombre = name;
    print("el nombre es: ${_user.nombre}");
  }

  onChangeLastName(String lastName) {
    _user.apellido = lastName;
    print("el apellido es: ${_user.apellido}");
  }

  onChangeDateOfBirth(String dateOfBirth) {
    _user.fechaDeNacimiento = dateOfBirth;
    print("el cumpleaños es: ${_user.fechaDeNacimiento}");
  }

  onChangeEmail(String email) {
    _user.email = email;
    print(_user.email);
  }

  // validaciones de todo falta
  updateUser() {
    status.value = StatusPerfil.wait;
    userRepository
        .updateUser(user)
        .then((value) => {
              status.value = StatusPerfil.ok,
              Future.delayed(Duration(seconds: 1))
                  .then((value) => status.value = StatusPerfil.none)
            })
        .catchError((_) {
      status.value = StatusPerfil.error;
    });
    print("esperando");
  }

  sendNumber() {
    print(_user.toJson());
    updateUser();
  }
}
