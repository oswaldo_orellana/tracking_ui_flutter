import 'package:get/get.dart';
import 'package:tracking_ui_flutter/app/data/repository/authentication_client.dart';
import 'package:tracking_ui_flutter/app/data/repository/local/local_number_repository.dart';
import 'package:tracking_ui_flutter/app/data/repository/user_repository.dart';
import 'package:tracking_ui_flutter/app/routes/app_routes.dart';

class SplashController extends GetxController {
  final _authenticationClientRepository = Get.find<AuthenticationRepository>();

  @override
  void onReady() {
    super.onReady();
    routePage();
  }

  void routePage() async {
    try {
      _authenticationClientRepository.accessToken != null
          ? loadUser()
          : Get.offAllNamed(Routes.LOGINFORM);
    } catch (e) {
      print(e);
    }
  }

  loadUser() async {
    String numero = await Get.find<LocalNumberRepository>().number;
    print("usuario : {$numero}");
    Get.find<UserRepository>().cargarUser(numero);
    Get.offAllNamed(Routes.HOME);
  }
}
