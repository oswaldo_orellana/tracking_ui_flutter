import 'dart:math';

import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tracking_ui_flutter/app/controller/home/home_controller.dart';
import 'package:tracking_ui_flutter/app/util/state.dart';
import 'package:maps_curved_line/maps_curved_line.dart';

class CtrlMapasController extends GetxController {
  LatLng _position = LatLng(-17.8146, -63.1561);
  CameraPosition _camera =
      CameraPosition(target: LatLng(-17.8146, -63.1561), zoom: 15.0);

  RxBool buscando = true.obs;
  Rx<Status> estado = Rx(Status.none);

  get camera => _camera;
  get position => _position;

  final Set<Marker> markers = Set();
  final Set<Polyline> polylines = Set();

  void _posNew(Position pos) => _position = LatLng(pos.latitude, pos.longitude);

  getLocation(GoogleMapController controller) {
    getPosition().then((value) {
      updateCamera(value);
      _posNew(value);
    }).then((ok) {
      controller.animateCamera(CameraUpdate.newLatLng(_position));
      Future.delayed(Duration(milliseconds: 1000), () {
        buscando.value = false;
      });
    });
    buscando.value = true;
  }

  Future<Position> getPosition() async {
    return await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.high,
    );
  }

  updateCamera(Position pos) {
    _camera = CameraPosition(
      target: LatLng(pos.latitude, pos.longitude),
      zoom: 15.0,
    );
  }

  onCameraMove(LatLng pos) {
    _position = pos;
  }

  // mover camera final
  getMoveCamera() {
    place().then((value) {
      Get.find<HomeController>().onChangeDireccion(value[0].name);
      buscando.value = false;
    });
    buscando.value = true;
  }

  Future<List<Placemark>> place() async {
    return await placemarkFromCoordinates(
        _position.latitude, _position.longitude);
  }

  addMarker(LatLng position, String title, String descripcion) {
    print("agregando marker");
    Marker marca = Marker(
      markerId: MarkerId(title),
      position: position,
      infoWindow: InfoWindow(
        title: title,
        snippet: descripcion,
      ),
      icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
    );
    markers.add(marca);
  }

  centerView(
    GoogleMapController controller,
    LatLng origen,
    LatLng destino,
  ) async {
    await controller.getVisibleRegion();

    var left = min(origen.latitude, destino.latitude);
    var rigth = max(origen.latitude, destino.latitude);
    var top = max(origen.longitude, destino.longitude);
    var buttom = min(origen.longitude, destino.longitude);

    var bounds = LatLngBounds(
      southwest: LatLng(left, buttom),
      northeast: LatLng(rigth, top),
    );

    controller.animateCamera(CameraUpdate.newLatLngBounds(bounds, 50));
  }

  agregarPolyline(
    LatLng origen,
    LatLng destino,
  ) {
    polylines.add(
      Polyline(
        polylineId: PolylineId("line 1"),
        visible: true,
        width: 6,
        // patterns: [PatternItem.dash(1000)],
        points: MapsCurvedLines.getPointsOnCurve(origen, destino),
        color: Colors.green,
        zIndex: -50,
      ),
    );
    polylines.add(
      Polyline(
        polylineId: PolylineId("Origen destino"),
        visible: true,
        width: 6,
        patterns: [PatternItem.dash(5)],
        points: [origen, destino],
        color: Colors.black26,
        zIndex: -100,
      ),
    );
  }

  cleanMarkerAndPolyline() {
    markers.clear();
    polylines.clear();
  }
}
