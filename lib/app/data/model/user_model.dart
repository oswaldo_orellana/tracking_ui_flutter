class Usuario {
  Usuario({
    this.apellido,
    this.email,
    this.fechaDeNacimiento,
    this.nombre,
    this.numero,
    this.imageUrl,
  });

  String apellido;
  String email;
  String fechaDeNacimiento;
  String nombre;
  String numero;
  String imageUrl;

  Usuario copyWith({
    String apellido,
    String email,
    String fechaDeNacimiento,
    String nombre,
    String numero,
    String imageUrl,
  }) =>
      Usuario(
        apellido: apellido ?? this.apellido,
        email: email ?? this.email,
        fechaDeNacimiento: fechaDeNacimiento ?? this.fechaDeNacimiento,
        nombre: nombre ?? this.nombre,
        numero: numero ?? this.numero,
        imageUrl: imageUrl ?? this.imageUrl,
      );

  factory Usuario.fromJson(Map<String, dynamic> json) => Usuario(
        apellido: json["apellido"] == null ? null : json["apellido"],
        email: json["email"] == null ? null : json["email"],
        fechaDeNacimiento: json["fechaDeNacimiento"] == null
            ? null
            : json["fechaDeNacimiento"],
        nombre: json["nombre"] == null ? null : json["nombre"],
        numero: json["numero"] == null ? null : json["numero"],
        imageUrl: json["imageUrl"] == null ? null : json["imageUrl"],
      );

  Map<String, dynamic> toJson() => {
        "apellido": apellido == null ? null : apellido,
        "email": email == null ? null : email,
        "fechaDeNacimiento":
            fechaDeNacimiento == null ? null : fechaDeNacimiento,
        "nombre": nombre == null ? null : nombre,
        "numero": numero == null ? null : numero,
        "imageUrl": imageUrl == null ? null : imageUrl,
      };
}
