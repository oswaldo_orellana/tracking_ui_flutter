import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:meta/meta.dart' show required;

class Pedido {
  Pedido({
    @required this.id,
    @required this.origen,
    @required this.destino,
    @required this.dataTime,
    @required this.idUser,
    @required this.titleOrigen,
    @required this.titleDestino,
    @required this.estado,
    @required this.estadoPedido,
  });

  final String id;
  final LatLng origen;
  final LatLng destino;
  final DateTime dataTime;
  final String idUser;
  final String titleOrigen;
  final String titleDestino;
  final String estado;
  final bool estadoPedido;

  Pedido copyWith({
    String id,
    LatLng origen,
    LatLng destino,
    String dataTime,
    String idUser,
    String titleOrigen,
    String titleDestino,
    String estado,
    bool estadoPedido,
  }) =>
      Pedido(
        id: id ?? this.id,
        origen: origen ?? this.origen,
        destino: destino ?? this.destino,
        dataTime: dataTime ?? this.dataTime,
        idUser: idUser ?? this.idUser,
        titleOrigen: titleOrigen ?? this.titleOrigen,
        titleDestino: titleDestino ?? this.titleDestino,
        estado: estado ?? this.estado,
        estadoPedido: estadoPedido ?? this.estado,
      );

  factory Pedido.fromJson(Map<String, dynamic> json, String id) => Pedido(
        id: id,
        origen: json["origen"] == null
            ? null
            : LatLng(json["origen"][0], json["origen"][1]),
        destino: json["destino"] == null
            ? null
            : LatLng(json["destino"][0], json["destino"][1]),
        dataTime:
            json["dataTime"] == null ? null : DateTime.parse(json["dataTime"]),
        idUser: json["idUser"] == null ? null : json["idUser"],
        titleOrigen: json["titleOrigen"] == null ? null : json["titleOrigen"],
        titleDestino:
            json["titleDestino"] == null ? null : json["titleDestino"],
        estado: json["estado"] == null ? null : json["estado"],
        estadoPedido:
            json["estadoPedido"] == null ? null : json["estadoPedido"],
      );

  Map<String, dynamic> toJson() => {
        //"id": id == null ? null : id,
        "origen": origen == null ? null : origen.toJson(),
        "destino": destino == null ? null : destino.toJson(),
        "dataTime": dataTime == null ? null : dataTime.toString(),
        "idUser": idUser == null ? null : idUser,
        "titleOrigen": titleOrigen == null ? null : titleOrigen,
        "titleDestino": titleDestino == null ? null : titleDestino,
        "estado": estado == null ? null : estado,
        "estadoPedido": estadoPedido == null ? null : estadoPedido,
      };
}
