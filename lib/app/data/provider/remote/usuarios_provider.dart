import 'package:get/get.dart';
import 'package:tracking_ui_flutter/app/data/model/user_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:meta/meta.dart';

class UserProvider {
  final referenceUser = FirebaseFirestore.instance.collection("usuario");
  var _usuario = Get.find<Usuario>();

  //Usuario get usuario => _usuario;

  Future<Usuario> getUsuario(String number) async {
    var snapshop = await referenceUser.doc(number).get();
    _usuario = Usuario.fromJson(snapshop.data());
    return _usuario;
  }

  Usuario get usuario => _usuario;

  Future<void> updateUser({@required Usuario user}) async {
    await referenceUser
        .doc(user.numero)
        .set(user.toJson())
        .whenComplete(() => actualizarVarUsuario(_usuario.numero));
    print("termino el update user");
  }

  Future<void> registerNumber({String number}) async {
    await referenceUser.doc(number).get().then((value) async {
      if (value.data() == null) {
        _usuario = Usuario(numero: number);
        await updateUser(
          user: _usuario,
        );
      } else {
        actualizarVarUsuario(number);
      }
    });
  }

  Future<void> actualizarVarUsuario(String number) async {
    await referenceUser.doc(number).get().then(
      (value) {
        print(number);
        print(value.data());
        _usuario = Usuario.fromJson(value.data());
      },
    );
  }
}

mixin UserMixin {
  void error(String error);
}
