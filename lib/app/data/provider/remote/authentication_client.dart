import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';

class AuthenticationClient {
  final FirebaseAuth _auth = Get.find<FirebaseAuth>();

  //get user of firebase
  User get user => _auth.currentUser;

  //get access token of firebase
  Future<String> get accessToken => user?.getIdToken();

  Future<void> signOut() {
    return _auth.signOut();
  }
}
