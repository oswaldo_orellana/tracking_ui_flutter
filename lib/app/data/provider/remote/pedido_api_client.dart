import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tracking_ui_flutter/app/data/model/pedido.dart';

final referencePedido = FirebaseFirestore.instance.collection("pedido");

class PedidoClient {
  Future<String> registrarPedido(String titleOrigen, LatLng origen,
      String titleDestino, LatLng destino, String number) {
    Pedido pedido = Pedido(
      idUser: number,
      titleOrigen: titleOrigen,
      titleDestino: titleDestino,
      origen: origen,
      destino: destino,
      estado: "requesting",
      dataTime: DateTime.now(),
      id: '',
      estadoPedido: true,
    );
    return referencePedido.add(pedido.toJson()).then((value) => value.id);
  }

  Future<String> registrarPedidoModel(Pedido pedido) {
    return referencePedido.add(pedido.toJson()).then((value) => value.id);
  }

  Future<Pedido> getPedidoUser(String numero) {
    return referencePedido
        .where('estadoPedido', isEqualTo: true)
        .where('idUser', isEqualTo: numero)
        .get()
        .then((_) {
      if (_.docs.length < 1) return null;
      return Pedido.fromJson(_.docs.first.data(), _.docs.first.id);
      //print(_.docs.first.data()["destino"]);
    });
  }

  Future<void> cancelarPedido(String id) {
    return referencePedido.doc(id).update({'estadoPedido': false,'estado': 'cancelled'});
  }

  Future<void> changeOrderStatus(String number, String orderStatus) {
    return referencePedido.doc(number).update({'estado': orderStatus});
  }

  Stream orderStatus(String number) {
    return referencePedido
        .where('idUser', isEqualTo: number)
        .where('estado')
        .where('estadoPedido', isEqualTo: true)
        .snapshots()
        .map((event) => event.docChanges.first.doc.data()['estado']);
  }
}
