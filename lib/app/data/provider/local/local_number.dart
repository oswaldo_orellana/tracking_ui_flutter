import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/instance_manager.dart';

class LocalNumber {
  static const KEY = "number";
  final FlutterSecureStorage _storage = Get.find<FlutterSecureStorage>();

  Future<void> setNumber(String number) async {
    await _storage.write(key: KEY, value: number);
  }

  Future<void> clearNumber() async {
    await _storage.delete(key: KEY);
  }

  Future<String> getNumber() async {
    final String data = await _storage.read(key: KEY);
    print(data);
    if (data != null) {
      return data;
    } else {
      return null;
    }
  }
}
