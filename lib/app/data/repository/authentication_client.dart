import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'package:tracking_ui_flutter/app/data/provider/remote/authentication_client.dart';

class AuthenticationRepository {
  final _client = Get.find<AuthenticationClient>();

  Future<String> get accessToken => _client.accessToken;

  User get user => _client.user;

  Future<void> signOut() {
    return _client.signOut();
  }
}
