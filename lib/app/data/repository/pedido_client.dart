import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tracking_ui_flutter/app/data/model/pedido.dart';
import 'package:tracking_ui_flutter/app/data/provider/remote/pedido_api_client.dart';

class PedidoRepository {
  PedidoClient pedidoProvider = Get.find<PedidoClient>();

  Future<String> add(
      {String titleOrigen,
      LatLng origen,
      String titleDestino,
      LatLng destino,
      String number}) {
    return pedidoProvider.registrarPedido(
        titleOrigen, origen, titleDestino, destino, number);
  }

  Future<String> addModel(Pedido pedido) {
    return pedidoProvider.registrarPedidoModel(pedido);
  }

  Future<Pedido> getPedidoUser(String number) {
    return pedidoProvider.getPedidoUser(number);
  }

  Future<void> cancelar(String id) {
    return pedidoProvider.cancelarPedido(id);
  }

  Stream orderStatus(String number) {
    return pedidoProvider.orderStatus(number);
  }

  Future<void> changeOrderStatus(String number, String orderStatus) {
    return pedidoProvider.changeOrderStatus(number, orderStatus);
  }
}
