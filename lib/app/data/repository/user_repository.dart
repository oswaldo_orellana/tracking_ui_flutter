import 'package:get/get.dart';
import 'package:tracking_ui_flutter/app/data/model/user_model.dart';
import 'package:tracking_ui_flutter/app/data/provider/remote/usuarios_provider.dart';

class UserRepository {
  final UserProvider repository = Get.find<UserProvider>();

  addNumber(String number) {
    repository.registerNumber(number: number);
  }

  addUser(String number) {}

  Future<void> updateUser(Usuario user) async {
    return repository.updateUser(user: user);
  }

  cargarUser(String number) {
    repository.actualizarVarUsuario(number);
  }

  Usuario get user => repository.usuario;

  getUser(String number) {
    return repository.getUsuario(number);
  }
}
