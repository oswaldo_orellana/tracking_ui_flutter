import 'package:get/get.dart';
import 'package:tracking_ui_flutter/app/data/provider/local/local_number.dart';

class LocalNumberRepository {
  final LocalNumber _localNumber = Get.find<LocalNumber>();
  Future<void> setNumber(String number) => _localNumber.setNumber(number);

  Future<void> clearNumber() => _localNumber.clearNumber();

  //Future<String> get number => _localNumber.getNumber();
  Future<String> get number {
    return _localNumber.getNumber();
  }
}
