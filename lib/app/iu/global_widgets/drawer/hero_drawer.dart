import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tracking_ui_flutter/app/data/repository/user_repository.dart';
import 'package:tracking_ui_flutter/app/routes/app_routes.dart';

class HeroDrawer extends StatelessWidget {
  final _user = Get.find<UserRepository>().user;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).pop();
        Get.toNamed(Routes.CONFIGURATIONPROFILE);
      },
      child: Container(
        padding: EdgeInsets.only(left: 20, right: 0, top: 15, bottom: 15),
        color: Colors.black,
        height: 80,
        child: Row(
          children: [
            CircleAvatar(
              radius: 27.0,
              backgroundImage: AssetImage('assets/images/userlogo.png'),
            ),
            SizedBox(
              width: 15,
            ),
            Container(
              width: MediaQuery.of(context).size.width / 3 + 30,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    (_user.nombre != null)
                        ? (_user.nombre.isNotEmpty)
                            ? _user.nombre
                            : 'nameempty'.tr
                        : 'nameempty'.tr,
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Colors.white,
                      fontSize: 18.0,
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(height: 10.0),
                  Text(
                    (_user.email != null)
                        ? (_user.email.isNotEmpty)
                            ? _user.email
                            : 'emailempty'.tr
                        : 'emailempty'.tr,
                    style: TextStyle(
                      fontWeight: FontWeight.normal,
                      color: Colors.white70,
                      fontSize: 12.0,
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ),
            ),
            IconButton(
              icon: Icon(
                Icons.chevron_right,
                color: Colors.white,
              ),
              onPressed: () {
                print("hola");
                print(_user.email);
              },
            )
          ],
        ),
      ),
    );
  }
}
