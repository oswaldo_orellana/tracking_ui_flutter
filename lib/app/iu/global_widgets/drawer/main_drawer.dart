import 'package:flutter/material.dart';
import 'hero_drawer.dart';

class MenuDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            HeroDrawer(),
            ListTile(
              leading: Icon(Icons.location_city),
              title: Text('Ciudad'),
            ),
            ListTile(
              leading: Icon(Icons.watch_later_outlined),
              title: Text('Mis viajes'),
            ),
            ListTile(
              leading: Icon(Icons.blur_circular),
              title: Text('Interurbano'),
            ),
            ListTile(
              leading: Icon(Icons.shield),
              title: Text('Seguridad y Protección'),
            ),
            ListTile(
              leading: Icon(Icons.settings),
              title: Text('Configuraciones'),
            ),
            ListTile(
              leading: Icon(Icons.info_outline),
              title: Text('Ayuda'),
            ),
            ListTile(
              leading: Icon(Icons.chat),
              title: Text('Soporte'),
            ),
          ],
        ),
      ),
    );
  }
}
