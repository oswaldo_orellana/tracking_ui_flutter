import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tracking_ui_flutter/app/controller/configuration_profile/configuration_profile_controller.dart';

class WaitingWidgetConf extends StatelessWidget {
  const WaitingWidgetConf({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ConfigurationProfileController>(
      builder: (_) => Obx(() {
        if (_.status.value == StatusPerfil.wait) {
          return Positioned(
            child: Container(
              height: Get.size.height,
              width: Get.size.width,
              color: Colors.black54,
              child: CupertinoActivityIndicator(
                radius: 20,
                animating: true,
              ),
            ),
          );
        }
        if (_.status.value == StatusPerfil.ok) {
          return Positioned(
            child: Container(
              color: Colors.black54,
              child: Center(
                  child: Text(
                "Succcesfull",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 30,
                ),
              )),
            ),
          );
        }
        if (_.status.value == StatusPerfil.error) {
          return Positioned(
            child: Container(
              color: Colors.black54,
              child: Center(
                  child: Text(
                "Tuvimos un problema intente nuevamente",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 30,
                ),
              )),
            ),
          );
        }
        return Container();
      }),
    );
  }
}
