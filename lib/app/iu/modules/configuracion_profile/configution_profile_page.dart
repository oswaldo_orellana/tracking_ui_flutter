import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tracking_ui_flutter/app/controller/configuration_profile/configuration_profile_controller.dart';
import 'package:tracking_ui_flutter/app/iu/modules/configuracion_profile/widget/waiting.dart';

class ConfProfilePage extends GetView<ConfigurationProfileController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.black,
        title: Text(
          'titleprofile'.tr,
          style: TextStyle(),
        ),
        shadowColor: Colors.white38,
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        color: Colors.black,
        height: Get.size.height,
        child: Stack(
          //alignment: Alignment.topCenter,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  height: 155.0,
                  alignment: Alignment.bottomCenter,
                  child: ClipOval(
                    child: Align(
                      alignment: Alignment.center,
                      child: Image.asset('assets/images/user.png'),
                    ),
                  ),
                ),
                SizedBox(
                  height: 50.0,
                ),
                Form(
                  child: Column(
                    children: [
                      TextFormField(
                        initialValue: controller.user.nombre,
                        style: TextStyle(
                          color: Colors.white,
                        ),
                        onChanged: controller.onChangeName,
                        autocorrect: true,
                        keyboardType: TextInputType.name,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            prefixIcon: Icon(
                              Icons.person,
                              color: Colors.white,
                              size: 19,
                            ),
                            hintText: 'name'.tr,
                            hintStyle: TextStyle(
                              color: Colors.white,
                            ),
                            labelStyle: TextStyle(color: Colors.white)),
                      ),
                      Divider(
                        color: Colors.white,
                        height: 3,
                      ),
                      TextFormField(
                        initialValue: controller.user.apellido,
                        style: TextStyle(
                          color: Colors.white,
                        ),
                        onChanged: controller.onChangeLastName,
                        autocorrect: true,
                        keyboardType: TextInputType.name,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            prefixIcon: Icon(
                              Icons.person,
                              size: 19,
                              color: Colors.white,
                            ),
                            hintText: 'lastname'.tr,
                            hintStyle: TextStyle(
                              color: Colors.white,
                            ),
                            labelStyle: TextStyle(color: Colors.white)),
                      ),
                      Divider(
                        color: Colors.white,
                        height: 3,
                      ),
                      TextFormField(
                        initialValue: controller.user.fechaDeNacimiento,
                        onChanged: controller.onChangeDateOfBirth,
                        style: TextStyle(
                          color: Colors.white,
                        ),
                        autocorrect: true,
                        keyboardType: TextInputType.datetime,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            prefixIcon: Icon(
                              Icons.calendar_today,
                              size: 19,
                              color: Colors.white,
                            ),
                            hintText: 'datebirth'.tr,
                            hintStyle: TextStyle(
                              color: Colors.white,
                            ),
                            labelStyle: TextStyle(color: Colors.white)),
                      ),
                      Divider(
                        color: Colors.white,
                        height: 3,
                      ),
                      TextFormField(
                        initialValue: controller.user.email,
                        onChanged: controller.onChangeEmail,
                        style: TextStyle(
                          color: Colors.white,
                        ),
                        autocorrect: true,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            prefixIcon: Icon(
                              Icons.email,
                              size: 19,
                              color: Colors.white,
                            ),
                            hintText: 'email'.tr,
                            hintStyle: TextStyle(
                              color: Colors.white,
                            ),
                            labelStyle: TextStyle(color: Colors.white)),
                      ),
                      Divider(
                        color: Colors.white,
                        height: 3,
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: MaterialButton(
                height: 40.0,
                minWidth: 200.0,
                onPressed: () {
                  FocusScope.of(context).unfocus();
                  controller.sendNumber();
                },
                color: Colors.green,
                child: Text(
                  'save'.tr,
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            WaitingWidgetConf(),
          ],
        ),
      ),
    );
  }
}
