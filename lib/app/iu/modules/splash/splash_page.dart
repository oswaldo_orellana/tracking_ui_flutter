import 'package:flutter/material.dart';
import 'package:get/state_manager.dart';
import 'package:tracking_ui_flutter/app/controller/splash/splash_controller.dart';

class SplashPage extends GetWidget<SplashController> {
  const SplashPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Text("hola mundo"),
      ),
    );
  }
}
