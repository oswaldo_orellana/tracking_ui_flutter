import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tracking_ui_flutter/app/controller/phone_authentication/phone_authentication_controller.dart';

class TextFieldMostrar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<PhoneAuthenticationController>(
      builder: (_) => Obx(
        () => _.mostrar.value
            ? TextFormField(
                keyboardType: TextInputType.number,
                maxLength: 6,
                onChanged: _.onSmsCodeChanged,
                decoration: InputDecoration(
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.black12,
                    ),
                  ),
                  hintText: 'textinputcode'.tr,
                  hintStyle: TextStyle(color: Colors.black26, fontSize: 14),
                ),
              )
            : Container(),
      ),
    );
  }
}
