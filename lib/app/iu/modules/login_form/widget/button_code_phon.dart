import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tracking_ui_flutter/app/controller/phone_authentication/phone_authentication_controller.dart';

class ButtonLoginPhone extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<PhoneAuthenticationController>(
      builder: (_) => Obx(
        // ignore: deprecated_member_use
        () => RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          onPressed: () {
            FocusScope.of(context).unfocus();
            !_.mostrar.value ? _.sendMeACode() : _.verifyCode();
          },
          color: Colors.green,
          child: Text(!_.mostrar.value ? 'next'.tr : 'verify'.tr),
        ),
      ),
    );
  }
}
