import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tracking_ui_flutter/app/controller/phone_authentication/phone_authentication_controller.dart';

class TextInputNumber extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<PhoneAuthenticationController>(
      builder: (_) => TextFormField(
        onChanged: _.onNumberChanged,
        keyboardType: TextInputType.phone,
        decoration: InputDecoration(
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: Colors.black12,
            ),
          ),
          hintText: 'textinputnumber'.tr,
          hintStyle: TextStyle(
            color: Colors.black26,
          ),
        ),
      ),
    );
  }
}
