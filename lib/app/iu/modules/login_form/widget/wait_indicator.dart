import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tracking_ui_flutter/app/controller/phone_authentication/phone_authentication_controller.dart';
import 'package:tracking_ui_flutter/app/util/state.dart';

class WaitIndicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<PhoneAuthenticationController>(
      builder: (_) => Obx(() {
        if (_.status.value == Status.wait) {
          return Positioned.fill(
            child: Container(
              color: Colors.white54,
              child: CupertinoActivityIndicator(),
            ),
          );
        } else {
          return Container();
        }
      }),
    );
  }
}
