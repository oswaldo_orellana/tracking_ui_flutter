import 'package:flutter/material.dart';
import 'package:tracking_ui_flutter/app/iu/modules/login_form/widget/button_code_phon.dart';
import 'package:tracking_ui_flutter/app/iu/modules/login_form/widget/text_field_input.dart';
import 'package:tracking_ui_flutter/app/iu/modules/login_form/widget/text_input_number.dart';
import 'package:tracking_ui_flutter/app/iu/modules/login_form/widget/wait_indicator.dart';
import 'package:get/get.dart';

class LoginFormPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: GestureDetector(
            onTap: () {
              FocusScope.of(context).unfocus();
            },
            child: Container(
              color: Colors.transparent,
              child: Stack(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Container(
                        padding: EdgeInsets.only(top: 25, left: 13, right: 13),
                        child: Center(
                            child: Text(
                          'titlelogin'.tr,
                          style: TextStyle(
                              fontSize: 22,
                              fontWeight: FontWeight.w400,
                              color: Colors.black87),
                          textAlign: TextAlign.center,
                        )),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 15, right: 20),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Row(
                              children: [
                                SizedBox(
                                  width: 40,
                                  height: 40,
                                  child: Image.asset(
                                      "assets/images/flag_bolivia.png"),
                                ),
                                SizedBox(
                                  width: 15,
                                ),
                                Expanded(
                                  child: TextInputNumber(),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 6,
                            ),
                            TextFieldMostrar(),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 30, vertical: 4),
                              child: ButtonLoginPhone(),
                            ),
                            SizedBox(
                              height: 7,
                            ),
                            Text(
                              'texttermcond'.tr,
                              style: TextStyle(
                                  color: Colors.black26, fontSize: 11.5),
                              textAlign: TextAlign.center,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  WaitIndicator(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
