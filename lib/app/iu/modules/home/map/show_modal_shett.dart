import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tracking_ui_flutter/app/controller/home/home_controller.dart';
import 'package:tracking_ui_flutter/app/routes/app_routes.dart';

class ShowModalSheet extends StatelessWidget {
  final double he;
  ShowModalSheet(this.he);
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: he,
      child: Container(
        //color: Colors.black,
        height: he / 2,
        padding: EdgeInsets.only(
          right: 15,
          left: 20,
          bottom: MediaQuery.of(context).viewInsets.bottom,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.all(2),
              child: Align(
                alignment: Alignment.topCenter,
                child: Container(
                  padding: EdgeInsets.all(3),
                  height: 5,
                  width: 30,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.black,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 1,
            ),
            TextField(
              style: TextStyle(
                color: Colors.black,
              ),
              decoration: InputDecoration(
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.black54,
                  ),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.black54,
                  ),
                ),
                icon: Icon(
                  Icons.circle,
                  size: 20,
                ),
                hintText: Get.find<HomeController>().tipo.value
                    ? "textplaceorigin".tr
                    : "textplacedestination".tr,
                hintStyle: TextStyle(
                    color: Colors.black54,
                    fontSize: 14.5,
                    fontWeight: FontWeight.w400),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Icon(
                  Icons.location_on,
                  color: Colors.green,
                  size: 25,
                ),
                SizedBox(
                  width: 15,
                ),
                GestureDetector(
                  onTap: () {
                    print(MediaQuery.of(context).padding.top);
                    Get.back();
                    Get.toNamed(Routes.SELECTMAP);
                  },
                  child: Text(
                    'showmap'.tr,
                    style: TextStyle(
                      color: Colors.green,
                      fontSize: 14.5,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
