import 'package:flutter/material.dart';

class IconoPosition extends StatelessWidget {
  final String lugar;
  const IconoPosition({Key key, this.lugar}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(100.0),
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.circle,
      ),
    );
  }
}
