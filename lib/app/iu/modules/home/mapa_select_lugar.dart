import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tracking_ui_flutter/app/controller/home/home_controller.dart';
import 'package:tracking_ui_flutter/app/controller/mapas/ctrl_mapas_controller.dart';
import 'package:tracking_ui_flutter/app/routes/app_routes.dart';

class MapaSelect extends StatefulWidget {
  @override
  _MapaSelectState createState() => _MapaSelectState();
}

class _MapaSelectState extends State<MapaSelect> {
  final map = Get.find<CtrlMapasController>();

  final home = Get.find<HomeController>();

  Container cabecera() {
    return Container(
      height: 50,
      width: 220,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        color: Colors.blue,
      ),
      //color: Colors.blue,
      child: Center(
        child: Obx(
          () => Text(
            home.direccion.value == "Unnamed Road"
                ? 'noaddressselect'.tr
                : home.direccion.value,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontSize: 15,
            ),
          ),
        ),
      ),
    );
  }

  Container wait() {
    return Container(
      height: 40,
      width: 40,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        color: Colors.blue,
      ),
      padding: EdgeInsets.all(10),
      child: CircularProgressIndicator(
        strokeWidth: 2.0,
        valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
      ),
    );
  }

  GoogleMapController _mapsControllerHome;

  GoogleMap mapasHome() {
    return GoogleMap(
      initialCameraPosition: map.camera,
      onMapCreated: (controller) {
        _mapsControllerHome = controller;
        map.getLocation(_mapsControllerHome);
      },
      onCameraMove: (position) {
        map.onCameraMove(position.target);
      },
      onCameraIdle: () {
        map.getMoveCamera();
      },
      myLocationEnabled: true,
      myLocationButtonEnabled: false,
      zoomControlsEnabled: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            height: Get.size.height - MediaQuery.of(context).padding.top,
            child: Stack(
              children: [
                mapasHome(),
                Align(
                  alignment: Alignment(0.0, -0.20),
                  child: Obx(
                    () => map.buscando.value ? wait() : cabecera(),
                  ),
                ),
                Align(
                  alignment: Alignment(0.0, -0.05),
                  child: Image.asset(
                    "assets/images/markeruser.png",
                    height: 40,
                  ),
                ),
                Positioned(
                  top: 15,
                  left: 15,
                  child: Container(
                    height: 45,
                    width: 45,
                    child: FloatingActionButton(
                      heroTag: "backPage",
                      backgroundColor: Colors.white,
                      child: Icon(
                        Icons.arrow_back,
                        color: Colors.green,
                        size: 25,
                      ),
                      onPressed: () => Get.back(),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 17),
                  child: Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                        width: (MediaQuery.of(context).size.width / 2) + 30,
                        child: Text(
                          home.tipo.value
                              ? 'titleoriginselect'.tr
                              : 'titledestselect'.tr,
                          style: TextStyle(
                              fontSize: 25, fontWeight: FontWeight.w700),
                          textAlign: TextAlign.center,
                        )),
                  ),
                ),
                Positioned(
                  bottom: 120,
                  right: 15,
                  child: FloatingActionButton(
                    heroTag: "2",
                    onPressed: () {
                      map.getLocation(_mapsControllerHome);
                      print(
                          "presionamos el boton ${_mapsControllerHome.mapId}");
                    },
                    mini: true,
                    backgroundColor: Colors.white,
                    child: Icon(
                      Icons.gps_fixed,
                      color: Colors.green,
                    ),
                  ),
                ),
                Positioned(
                  left: 15,
                  bottom: 15,
                  right: 15,
                  top: 15,
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 10),
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: SizedBox(
                        width: MediaQuery.of(context).size.width,
                        height: 48,
                        // ignore: deprecated_member_use
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30)),
                          onPressed: () =>
                              (home.direccion.value == "Unnamed Road")
                                  ? Get.toNamed(Routes.DIRNOENCONTRADA,
                                      arguments: map.position)
                                  : {
                                      home.guardarDireccion(
                                          map.position, home.direccion.value),
                                      Get.offAllNamed(Routes.HOME),
                                    },
                          color: Colors.green,
                          child: Obx(
                            () => Text(
                              home.direccion.value == "Unnamed Road"
                                  ? 'next'.tr
                                  : 'done'.tr,
                              style: TextStyle(
                                  color: Colors.white, fontSize: 19.0),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class DirNoEncontrada extends StatelessWidget {
  final home = Get.find<HomeController>();
  final position = Get.arguments;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          height: Get.size.height - MediaQuery.of(context).padding.top,
          child: Column(
            children: [
              Expanded(
                child: Stack(
                  children: [
                    Positioned(
                      left: 15,
                      top: 15,
                      child: FloatingActionButton(
                        mini: true,
                        child: Icon(
                          Icons.arrow_back,
                          color: Colors.green,
                        ),
                        onPressed: () {
                          Get.back();
                        },
                        backgroundColor: Colors.white,
                      ),
                    ),
                    Positioned(
                      child: Padding(
                        padding: const EdgeInsets.all(20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 40, top: 20, right: 40),
                              child: Center(
                                child: Text(
                                  'titleaddrespagenot'.tr,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 25,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            TextField(
                              maxLines: 5,
                              autofocus: true,
                              onChanged: home.onChangeDireccion,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                hintText: 'adddireccion'.tr,
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Colors.green,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            // ignore: deprecated_member_use
                            Obx(() => RaisedButton(
                                  disabledColor: Colors.grey,
                                  onPressed: (home.direccion.value ==
                                              "Unnamed Road" ||
                                          home.direccion.value.isEmpty)
                                      ? null
                                      : () {
                                          home.guardarDireccion(
                                              position, home.direccion.value);
                                          Get.offAllNamed(Routes.HOME);
                                        },
                                  child: Text('done'.tr),
                                )),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
