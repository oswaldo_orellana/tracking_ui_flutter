import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tracking_ui_flutter/app/controller/home/home_controller.dart';
import 'package:tracking_ui_flutter/app/controller/mapas/ctrl_mapas_controller.dart';
import 'package:tracking_ui_flutter/app/iu/global_widgets/drawer/main_drawer.dart';
import 'package:tracking_ui_flutter/app/util/state.dart' as estado;
import 'package:tracking_ui_flutter/app/util/state.dart';

import 'map/show_modal_shett.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  final home = Get.find<HomeController>();

  final map = Get.find<CtrlMapasController>();

  GoogleMapController _mapsControllerHome;

  GoogleMap mapasHome() {
    return GoogleMap(
      initialCameraPosition: map.camera,
      onMapCreated: (controller) {
        _mapsControllerHome = controller;

        if (home.origen != null && home.destino != null) {
          map.centerView(controller, home.origen, home.destino);
        } else {
          map.getLocation(_mapsControllerHome);
        }
      },
      onCameraMove: (position) {
        map.onCameraMove(position.target);
      },
      onCameraIdle: () {
        map.getMoveCamera();
      },
      myLocationEnabled: true,
      myLocationButtonEnabled: false,
      zoomControlsEnabled: false,
      markers: map.markers,
      polylines: map.polylines,
    );
  }

  void displayBottomSheet(BuildContext context) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.white,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        builder: (context) {
          return ShowModalSheet(he);
        });
  }

  Positioned signOutIzquierdo() {
    return Positioned(
      top: 10,
      right: 10,
      child: FloatingActionButton(
        heroTag: "exit",
        mini: true,
        onPressed: home.signOut,
        backgroundColor: Colors.white,
        child: Icon(
          Icons.share,
          color: Colors.green,
          size: 25,
        ),
      ),
    );
  }

  Positioned drawer() {
    return Positioned(
      top: 10,
      left: 10,
      child: FloatingActionButton(
        heroTag: "drawer",
        mini: true,
        onPressed: () {
          if (scaffoldKey.currentState.isDrawerOpen) {
            scaffoldKey.currentState.openEndDrawer();
          } else {
            scaffoldKey.currentState.openDrawer();
          }
        },
        backgroundColor: Colors.white,
        child: Icon(
          Icons.menu,
          color: Colors.green,
          size: 25,
        ),
      ),
    );
  }

  Positioned gps() {
    return Positioned(
      right: 10,
      bottom: 10,
      child: FloatingActionButton(
        onPressed: () {
          map.getLocation(_mapsControllerHome);
          print("presionamos el boton ${_mapsControllerHome.mapId}");
        },
        mini: true,
        heroTag: "gps",
        backgroundColor: Colors.white,
        child: Icon(
          Icons.gps_fixed,
          color: Colors.green,
          size: 28,
        ),
      ),
    );
  }

  Positioned asd() {
    return Positioned(
      left: 10,
      bottom: 10,
      child: FloatingActionButton(
        onPressed: () {
          print(Get.deviceLocale.toString());
          //home.onchangestado();
        },
        mini: true,
        heroTag: "aaaa",
        backgroundColor: Colors.white,
        child: Icon(
          Icons.gps_fixed,
          color: Colors.green,
          size: 28,
        ),
      ),
    );
  }

  double he;
  @override
  Widget build(BuildContext context) {
    he =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    double wi = double.infinity;
    return Scaffold(
      key: scaffoldKey,
      drawer: MenuDrawer(),
      body: SingleChildScrollView(
        child: SafeArea(
          child: SizedBox(
            width: wi,
            height: he,
            child: Stack(
              children: [
                Positioned.fill(
                  child: Column(
                    children: [
                      Container(
                        height: MediaQuery.of(context).size.height / 1.7,
                        child: Stack(
                          children: [
                            GetBuilder<HomeController>(
                              id: "mapas",
                              builder: (_) {
                                return mapasHome();
                              },
                            ),
                            Obx(() {
                              if (home.statusPedido.value == OrderStatus.none) {
                                return Align(
                                  alignment: Alignment(0.0, -0.08),
                                  child: Image.asset(
                                    "assets/images/markeruser.png",
                                    height: 40,
                                  ),
                                );
                              } else {
                                return Container();
                              }
                            }),
                            signOutIzquierdo(),
                            drawer(),
                            gps(),
                            asd(),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                height: 30,
                                width: double.infinity,
                                decoration: BoxDecoration(
                                  color: Colors.green[600].withOpacity(0.9),
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10.0),
                                    topRight: Radius.circular(10.0),
                                  ),
                                ),
                                child: Center(
                                  child: Obx(
                                    () => Text(
                                      (home.statusPedido.value ==
                                              OrderStatus.requesting)
                                          ? 'requesttitle'.tr
                                          : ((home.statusPedido.value ==
                                                  OrderStatus.inProgress))
                                              ? 'progresstitle'.tr
                                              : 'normaltitle'.tr,
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Obx(() => orderState()),
                              ),
                              //orderMenu(context),
                              Padding(
                                padding: const EdgeInsets.only(
                                    bottom: 20, left: 30, right: 30),
                                child: Obx(
                                  () => SizedBox(
                                    width: double.infinity,
                                    // ignore: deprecated_member_use
                                    child: RaisedButton(
                                      color: (home.statusPedido.value ==
                                                  OrderStatus.requesting ||
                                              home.statusPedido.value ==
                                                  OrderStatus.inProgress)
                                          ? Colors.red
                                          : Colors.green,
                                      onPressed: () {
                                        (home.statusPedido.value ==
                                                    OrderStatus.requesting ||
                                                home.statusPedido.value ==
                                                    OrderStatus.inProgress)
                                            ? Get.dialog(AlertDialog(
                                                title: Text(
                                                    'titlemodalcancelorder'.tr),
                                                content: Text(
                                                    'messagemodalcancelorder'
                                                        .tr),
                                                actions: [
                                                  TextButton(
                                                    onPressed: () {
                                                      Get.back();
                                                    },
                                                    child: Text(
                                                      'cancel'.tr,
                                                      style: TextStyle(
                                                        color:
                                                            Color(0xFF6200EE),
                                                      ),
                                                    ),
                                                  ),
                                                  TextButton(
                                                    onPressed: () {
                                                      home.cancelar();
                                                      Get.back();
                                                    },
                                                    child: Text(
                                                      'ok'.tr,
                                                      style: TextStyle(
                                                        //color: Color(0xFF6200EE),
                                                        color: Colors.red,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ))
                                            : home.add();
                                      },
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(30),
                                      ),
                                      child: Text(
                                        (home.statusPedido.value ==
                                                    OrderStatus.requesting ||
                                                home.statusPedido.value ==
                                                    OrderStatus.inProgress)
                                            ? 'cancelrequestbutton'.tr
                                            : 'requestmotobutton'.tr,
                                        style: TextStyle(
                                          fontSize: 17.0,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                estadoScreen(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget orderRequesting() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Image.asset(
              "assets/images/clockwaiting.gif",
            ),
          ),
          Text(
            'staterequest'.tr,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 24.0, color: Colors.green),
          ),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  Widget inProgress() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Image.asset(
              "assets/images/inprogress.gif",
            ),
          ),
          Text(
            'stateprogres'.tr,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 24.0, color: Colors.green),
          ),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  Widget orderMenu(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 22,
          vertical: 5,
        ),
        child: Column(
          //crossAxisAlignment: CrossAxisAlignment.stretch,
          //mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              height: 40,
              child: Obx(
                () => GestureDetector(
                  onTap: () {
                    home.tipo.value = true;
                    displayBottomSheet(context);
                  },
                  child: TextField(
                    enabled: false,
                    decoration: InputDecoration(
                      icon: Icon(
                        Icons.motorcycle,
                        size: 20,
                      ),
                      hintText: home.direccionOrigen.isEmpty
                          ? map.buscando.value
                              ? 'statecharging'.tr
                              : home.direccion.value
                          : home.direccionOrigen.toUpperCase(),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              height: 40,
              child: GestureDetector(
                onTap: () {
                  home.tipo.value = false;
                  displayBottomSheet(context);
                },
                child: Obx(() => TextField(
                      enabled: false,
                      decoration: InputDecoration(
                        icon: Icon(Icons.location_on),
                        hintText: home.direccionDestino.isEmpty
                            ? 'destination'.tr
                            : home.direccionDestino.toUpperCase(),
                        hintStyle: TextStyle(
                            color: Colors.black54,
                            fontSize: 14.5,
                            fontWeight: FontWeight.w400),
                      ),
                    )),
              ),
            ),
            Container(
              height: 40,
              child: TextField(
                onTap: () {},
                enabled: false,
                decoration: InputDecoration(
                    icon: Icon(
                      Icons.attach_money,
                      size: 21,
                    ),
                    hintText: 'rate'.tr,
                    hintStyle: TextStyle(
                        color: Colors.black54,
                        fontSize: 14.5,
                        fontWeight: FontWeight.w400)),
              ),
            ),
            Container(
              height: 40,
              child: TextField(
                onTap: () {},
                enabled: false,
                decoration: InputDecoration(
                    icon: Icon(
                      Icons.insert_comment,
                      size: 21,
                    ),
                    hintText: 'comment'.tr,
                    hintStyle: TextStyle(
                        color: Colors.black54,
                        fontSize: 14.5,
                        fontWeight: FontWeight.w400)),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget orderState() {
    if (OrderStatus.requesting == home.statusPedido.value) {
      return orderRequesting();
    }
    if (OrderStatus.inProgress == home.statusPedido.value) {
      return inProgress();
    }
    return orderMenu(Get.context);
  }

  Obx estadoScreen() {
    return Obx(() {
      if (home.status == estado.Status.remove) {
        return Positioned.fill(
          child: Container(
              color: Colors.white54,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Text(
                    'statecanceling'.tr,
                    textAlign: TextAlign.center,
                  ),
                  CupertinoActivityIndicator(),
                ],
              )),
        );
      }
      if (home.status == estado.Status.wait) {
        return Positioned.fill(
          child: Container(
            color: Colors.white54,
            child: CupertinoActivityIndicator(),
          ),
        );
      }
      if (home.status == estado.Status.ok) {
        return Positioned.fill(
          child: Container(
            color: Colors.white54,
            child: Center(child: Text('statesuccesfull'.tr)),
          ),
        );
      }
      if (home.status == estado.Status.error) {
        return Positioned.fill(
          child: Container(
            color: Colors.white54,
            child: Center(child: Text('stateerror'.tr)),
          ),
        );
      }
      return Container();
    });
  }
}
