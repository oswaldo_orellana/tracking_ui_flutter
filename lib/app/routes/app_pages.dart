import 'package:get/get.dart';
import 'package:tracking_ui_flutter/app/bindings/configution_profile_binding.dart';
import 'package:tracking_ui_flutter/app/bindings/home_binding.dart';
import 'package:tracking_ui_flutter/app/bindings/pedidos_binding.dart';
import 'package:tracking_ui_flutter/app/bindings/phone_authentication_binding.dart';
import 'package:tracking_ui_flutter/app/bindings/splash_binding.dart';
import 'package:tracking_ui_flutter/app/iu/modules/configuracion_profile/configution_profile_page.dart';
import 'package:tracking_ui_flutter/app/iu/modules/home/home_page.dart';
import 'package:tracking_ui_flutter/app/iu/modules/home/mapa_select_lugar.dart';
import 'package:tracking_ui_flutter/app/iu/modules/login_form/login_form_page.dart';
import 'package:tracking_ui_flutter/app/iu/modules/splash/splash_page.dart';
import 'package:tracking_ui_flutter/app/routes/app_routes.dart';

class AppPages {
  static final pages = [
    GetPage(
      name: Routes.INITIAL,
      page: () => SplashPage(),
      binding: SplashBinding(),
    ),
    GetPage(
      name: Routes.LOGINFORM,
      page: () => LoginFormPage(),
      binding: PhoneAuthenticationBinding(),
    ),
    GetPage(
        name: Routes.HOME,
        page: () => HomePage(),
        bindings: [PedidosBinding(), HomeBinding()]),
    GetPage(
      name: Routes.CONFIGURATIONPROFILE,
      page: () => ConfProfilePage(),
      binding: ConfigurationProfileBinding(),
    ),
    GetPage(
      name: Routes.SELECTMAP,
      page: () => MapaSelect(),
    ),
    GetPage(
      name: Routes.DIRNOENCONTRADA,
      page: () => DirNoEncontrada(),
    ),
  ];
}
