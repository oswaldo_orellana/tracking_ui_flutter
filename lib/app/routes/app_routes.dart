abstract class Routes {
  static const INITIAL = '/';
  static const SPLASH = 'splash';
  static const HOME = 'home';
  static const LOGINFORM = 'loginform';
  static const CONFIGURATIONPROFILE = 'configurationprofile';
  static const SELECTMAP = 'selectmap';
  static const DIRNOENCONTRADA = 'noencontrada';
}
