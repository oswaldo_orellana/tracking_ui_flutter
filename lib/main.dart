import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:tracking_ui_flutter/app/bindings/splash_binding.dart';
import 'package:tracking_ui_flutter/app/util/app_translate.dart';
import 'package:tracking_ui_flutter/app/util/injection_dependency.dart';

import 'app/iu/modules/splash/splash_page.dart';
import 'app/routes/app_pages.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  DependencyInjection.init();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      translationsKeys: AppTranslation.translationKeys,
      locale: Get.deviceLocale,
      fallbackLocale: Locale('es', 'BO'),
      debugShowCheckedModeBanner: false,
      title: 'Tracking',
      theme: ThemeData(
        buttonTheme: ButtonThemeData(
          buttonColor: Colors.green[600],
          height: 45,
          textTheme: ButtonTextTheme.primary,
        ),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: SplashPage(),
      initialBinding: SplashBinding(),
      getPages: AppPages.pages,
    );
  }
}
